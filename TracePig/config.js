import Config from 'react-native-config';

const PROTOCOL = Config.PROTOCOL;
const HOST = Config.HOST;
const PORT = Config.PORT;
const BASE_URL = Config.BASE_URL;

export default {
  URL: PROTOCOL + '://' + HOST + ':' + PORT + '/' + BASE_URL + '/'
}