import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import utils from '../../../helper/utils'

export default class ListDivider extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.dividerStyle]}>
        {this.props.dividerText && this.props.dividerText !== '' ?
          <Text style={[styles.text, this.props.dividerTextStyle]}>{this.props.dividerText.toUpperCase()}</Text>
          : null
        }
        {this.props.children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: 40,
    backgroundColor: '#5aaa4e',
    justifyContent: 'center',
    padding: 15
  },
  text: {
    fontSize: 16,
    color: '#fff',
    fontFamily: utils.fonts.nunitoBold
  }
});
