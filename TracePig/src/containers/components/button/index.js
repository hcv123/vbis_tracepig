'use strict';
import React, { PureComponent } from 'react';
import { TouchableHighlight, Text, StyleSheet, Dimensions, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import utils from '../../../helper/utils';

export default class Button extends PureComponent {
  onBtnPress = (e) => {
    this.props.onPress && this.props.onPress();
  }
  _renderButton() {
    return (
      <TouchableHighlight
        onPress={this.onBtnPress.bind(this)}
        style={[styles.button, this.props.buttonStyles]}
        underlayColor="#23ed74">
        <View style={styles.btnContainer}>
          {this.props.iconLeft ? <Icon style={styles.iconLeft} name={this.props.iconLeft} size={25} color={this.props.iconColor || '#fff'} /> : null}
          <View style={this.props.iconLeft ? [styles.textWrap, {marginLeft: 25}] : styles.textWrap}>
            <Text style={[styles.labelText, this.props.labelStyles]}>{this.props.upperCaseLabel ? this.props.label.toUpperCase() : this.props.label}</Text>
          </View>
          {this.props.iconRight ? <Icon style={styles.iconRight} name={this.props.iconRight} size={25} color={this.props.iconColor || '#fff'} /> : null}
        </View>
      </TouchableHighlight>
    )
  }
  render() {
    return this._renderButton();
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'green',
    height: 50,
    justifyContent: 'center',
    borderRadius: 5
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  textWrap: {
    minWidth: 100
  },
  labelText: {
    color: 'white',
    fontSize: 17,
    fontFamily: utils.fonts.nunitoBold
  },
  iconLeft: {
    position: 'absolute',
    minWidth: 10,
    left: 0,
    marginRight: 25,
    padding: 5
  },
  iconRight: {
    position: 'absolute',
    minWidth: 10,
    marginLeft: 25,
    right: 0, 
    padding: 5
  }
});

