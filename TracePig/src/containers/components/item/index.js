import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Dimensions } from 'react-native';
import utils from '../../../helper/utils';

export default class Item extends React.PureComponent {
  state = {
    numberOfLines: 2
  }
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }
  onPress(e) {
    if (this.props.onItemPress) {
      this.props.onItemPress();
    }
    if (this.props.numberOfLinesChanged) {
      this.setState({ numberOfLines: this.props.numberOfLinesChanged })
    }
  }
  render() {
    return (
      <TouchableHighlight underlayColor="#e5e5e8" onPress={this.onPress}>
        <View>
          <View style={styles.itemWrap}>
            {
              this.props.label ? (<Text style={styles.labelText}>{this.props.label + ':'}</Text>) : null
            }
            <Text
              style={styles.textItem}
              numberOfLines={2}>
              {this.props.value || '---'}
            </Text>
          </View>
          <View style={styles.separate} />
        </View>
      </TouchableHighlight>
    )
  }
}
const styles = StyleSheet.create({
  itemWrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 15,
  },
  labelText: {
    fontSize: 16,
    color: 'black',
    marginRight: 10,
    fontFamily: utils.fonts.nunitoBold
  },
  textItem: {
    fontSize: 16,
    paddingRight: 10,
    color: '#343435',
    fontFamily: utils.fonts.nunitoRegular
  },
  separate: {
    marginLeft: 15,
    borderTopWidth: 1,
    borderTopColor: '#ededed'
  }
});