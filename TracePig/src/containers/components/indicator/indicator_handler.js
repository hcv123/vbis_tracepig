import { DeviceEventEmitter } from 'react-native';

const handleLoader = {
  showIndicator: () => {
    DeviceEventEmitter.emit('changeLoading', {isVisible: true})
  },
  hideIndicator: () => {
    DeviceEventEmitter.emit('changeLoading', {isVisible: false})
  }
}

export default handleLoader