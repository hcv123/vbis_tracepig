import React from 'react';
import { View, Text, ActivityIndicator, DeviceEventEmitter, StyleSheet } from 'react-native';
import utils from '../../../helper/utils'

export default class BusyIndicator extends React.PureComponent {
  state = {
    isVisible: false,
    text: ''
  }
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.emiter = DeviceEventEmitter.addListener('changeLoading', this.changeLoadingEffect.bind(this))
  }

  componentWillUnmount() {
    this.emiter.remove()
  }

  changeLoadingEffect(state) {
    this.setState({
      isVisible: state.isVisible,
      text: this.props.text
    })
  }

  render() {
    if(this.state.isVisible) {
      return (
        <View style={this.props.showBackdrop ? [styles.container, styles.bg] : styles.container}>
          <View style={this.props.showBackdrop ? styles.overlay: {}}>
            <ActivityIndicator size="large" color="green" style={styles.progressBar}/>
            <Text style={this.props.showBackdrop ? styles.loadingText : {}}>{this.props.text}</Text>
          </View>
        </View>
      ) 
    } else {
      return null
    }
    
  }
}

BusyIndicator.defaultProps = {

}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  bg: {
    backgroundColor: 'rgba(255,255,255,0.3)'
  },
  progressBar: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  overlay: {
    width: 185,
    height: 100,
    backgroundColor: 'rgba(255,255,255,0.7)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  }, 
  loadingText: {
    color: 'green', 
    fontSize: 16,
    marginTop: 5,
    fontFamily: utils.fonts.nunitoRegular
  }
})
