import React, { Component } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet, Animated, Easing } from 'react-native';
import utils from '../../../helper/utils'

export default class ConnectionAlert extends Component {
    state = {
        isInternetConnection: true,
        postionViewValue: new Animated.Value(-500)
    }

    collapseAlertView(isConnected) {
        if(!isConnected) {
            Animated.timing(this.state.postionViewValue, {
                toValue: 0,
                duration: 3000,
                easing: Easing.ease
            }).start();
        } else {
            Animated.timing(this.state.postionViewValue, {
                toValue: -500,
                duration: 3000,
                easing: Easing.ease
            }).start();
        }
    }

    componentDidMount() {  
        NetInfo.isConnected.fetch().then(isConnected => {
            this.collapseAlertView(isConnected);
            this.setState({ ...this.state, isInternetConnection: isConnected })
        })
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange.bind(this)
        )
    }
    handleFirstConnectivityChange(isConnected) {
        // console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
        this.collapseAlertView(isConnected);
        this.setState({ ...this.state, isInternetConnection: isConnected })
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        )
    }
    render() {
        const bottom = this.state.postionViewValue;
        return (
            <Animated.View style={{ position: 'absolute', bottom}}>
                {
                    !this.state.isInternetConnection ? (
                        <View style={styles.container}>
                            <Text style={styles.alertText}>Không có kêt nối Internet.</Text>
                        </View>
                    ) : null
                }
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    container: {  
        backgroundColor: 'red', 
        width: Dimensions.get('window').width,  
        alignItems: 'center', 
        justifyContent: 'center', 
        padding: 5
    }, 
    alertText: {
        color: 'white',
        fontFamily: utils.fonts.nunitoRegular
    }
})

