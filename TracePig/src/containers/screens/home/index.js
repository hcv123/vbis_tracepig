import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, Alert } from 'react-native';

import Camera from 'react-native-camera';
import Icon from 'react-native-vector-icons/Ionicons'
import QRCodeScanner from 'react-native-qrcode-scanner';
import { Navigation } from 'react-native-navigation'

import { navigatorStyle } from '../../../styles';

import BusyIndicator from '../../components/indicator';
import ConnectionAlert from '../../components/internet_connection_alert';
import Button from '../../components/button';

import barcodeService from '../../../services/barcode.service';

import utils from '../../../helper/utils'
import config from '../../../../config'

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeScanner: true
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'willAppear':
        break;
      case 'didAppear':
        this.setState({ ...this.state, activeScanner: true });
        break;
      case 'willDisappear':
        break;
      case 'didDisappear':
        break;
      case 'willCommitPreview':
        break;
    }
  }


  async onBarCodeRead(e) {
    if (e.data) {
      this.setState({ ...this.state, activeScanner: false });
      const barcodeInfo = await barcodeService.getBarcodeInfo(e.data);
      if (barcodeInfo && barcodeInfo['items'].length) {
        this.props.navigator.push({
          screen: "app.BarCodeInfo",
          title: 'Thông tin truy xuất',
          navigatorStyle: navigatorStyle,
          passProps: { barcodeData: barcodeInfo['items'] }
        });
      } else {
        Alert.alert(
          "Opps.",
          "Mã không hợp lệ.",
          [
            { text: 'Bỏ qua', onPress: () => { this.setState({ ...this.state, activeScanner: true });} }
          ]
        )
      }

    }
  }

  render() {
    const topContent = (
      <View style={styles.logoContainer}>
        <Image source={require('../../../images/logo-deheus.png')}
          resizeMode="contain" style={{ width: 190, height: 110 }} />
      </View>
    )
    const bottomContent = (
      <View style={styles.bottomContainer}>
        <Button label="Tìm kiếm cửa hàng" upperCaseLabel={true} buttonStyles={{ height: 40, marginBottom: 5}} iconLeft="ios-locate" onPress={() => null}></Button>
        <Button label="Thông tin chuỗi cung ứng" upperCaseLabel={true} buttonStyles={{ height: 40}} iconLeft="ios-cart" onPress={() => null}></Button>
      </View>
    )

    return (
      <View style={styles.container}>
        {
          this.state.activeScanner ? (
            <QRCodeScanner
              topContent={topContent}
              fadeIn={true}
              onRead={this.onBarCodeRead.bind(this)}
              showMarker={true}
              reactivateTimeout={3000}
              bottomContent={bottomContent}
              checkAndroid6Permissions={true} />
          ) : null
        }
        <ConnectionAlert />
        <BusyIndicator text="Đang truy xuất..." showBackdrop={true} />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logoContainer: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomContainer: {
    justifyContent: 'center'
  }
});