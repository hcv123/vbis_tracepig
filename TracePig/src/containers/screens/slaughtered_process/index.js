import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';


import Item from '../../components/item';
import BusyIndicator from '../../components/indicator';

import slaughterService from '../../../services/slaughter.service'

import utils from '../../../helper/utils'

export default class processing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      processing: []
    };
  }
  componentDidMount() {
    this.getSlaughteredProcess()
  }
  async getSlaughteredProcess() {
    const response = await slaughterService.getSlaughteredProcess(this.props.code);
    const processing = response.items;
    Array.isArray(processing) && this.setState({ processing });
  }
  render() {
    return (
      <View style={styles.container}>
        <BusyIndicator showBackdrop={false} />
        <FlatList
          data={this.state.processing}
          keyExtractor={(item, index) => index}
          renderItem={({ item }) => (
            <View>
              <Item label="Địa chỉ GM" value={item.address} />
              <Item label="Nhân viên" value={item.nhan_vien} />
              <Item label="Số lượng" value={item.so_luong} />
              <Item label="Thời gian bắt đầu" value={utils.dateTimeFormat(item.start_time)} />
              <Item label="Thời gian kết thúc" value={utils.dateTimeFormat(item.end_time)} />
            </View>
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  }
})