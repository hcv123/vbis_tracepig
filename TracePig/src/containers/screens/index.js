import { Navigation } from 'react-native-navigation';
import HomeScreen from './home';
import BarCodeInfo from './barcode_info';
import BreedingProcess from './breeding_process';
import SlaughteredProcess from './slaughtered_process';

export function registerScreens() {
  Navigation.registerComponent('app.HomeScreen', () => HomeScreen);
  Navigation.registerComponent('app.BarCodeInfo', () => BarCodeInfo);
  Navigation.registerComponent('app.BreedingProcess', () => BreedingProcess);
  Navigation.registerComponent('app.SlaughteredProcess', () => SlaughteredProcess);
}