import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

import { navigatorStyle } from '../../../styles';

import Button from '../../components/button';
import Item from '../../components/item';
import ListDivider from '../../components/list_divider'

import request from '../../../services/request';
import barcodeService from '../../../services/barcode.service';

import utils from '../../../helper/utils';

export default class BarCodeInfo extends Component {
  lbCollections = {
    TRANGTRAI: {
      title: 'Trang trại',
      name: 'Tên trang trại',
      address: 'Địa chỉ',
      date: 'Ngày xuất',
      expireDate: null,
      number: 'Kiểm dịch số',
      buttonLabel: 'Quá trình chăn nuôi',
      event: (e) => {
        this.props.navigator.push({
          screen: 'app.BreedingProcess',
          title: 'Quá trình chăn nuôi',
          navigatorStyle: navigatorStyle,
          backButtonTitle: 'Back',
          passProps: { code: e }
        });
      },
      hasDetails: true
    },
    GIETMO: {
      title: 'Cơ sở giết mổ',
      name: 'Tên cơ sở',
      address: 'Địa chỉ',
      date: 'Ngày giết mổ',
      expireDate: null,
      number: 'Kiểm dịch số',
      buttonLabel: 'Quá trình giết mổ',
      event: (e) => {
        this.props.navigator.push({
          screen: 'app.SlaughteredProcess',
          title: 'Quá trình giết mổ',
          navigatorStyle: navigatorStyle,
          backButtonTitle: 'Back',
          passProps: { code: e }
        });
      },
      hasDetails: true
    },
    BANLE: {
      title: 'Bán lẻ',
      name: 'Tên cơ sở',
      address: 'Địa chỉ',
      date: 'Ngày xuất',
      expireDate: 'Ngày hết hạn',
      number: 'Kiểm dịch số',
      buttonLabel: '',
      event: (e) => { },
      hasDetails: false
    },
    BANSI: {
      title: 'Bán sỉ',
      name: 'Tên cơ sở',
      address: 'Địa chỉ',
      date: 'Ngày xuất',
      expireDate: 'Ngày hết hạn',
      number: 'Kiểm dịch số',
      buttonLabel: '',
      event: (e) => { },
      hasDetails: false
    }
  };
  constructor(props) {
    super(props);
  }

  getCollection(key) {
    return this.lbCollections[key] || {
      title: 'Thông tin',
      name: 'Tên',
      address: 'Địa chỉ',
      date: 'Ngày',
      expireDate: null,
      number: 'Số',
      buttonLabel: '',
      event: (e) => { }
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.barcodeData}
          keyExtractor={(item, index) => index}
          renderItem={({ item }) => (
            <View>
              <ListDivider dividerText={this.getCollection(item.org_type).title}>
                {
                  this.getCollection(item.org_type).hasDetails ? (
                    <Button
                      label={this.getCollection(item.org_type).buttonLabel}
                      onPress={this.getCollection(item.org_type).event.bind(this, item.qr_code)}
                      buttonStyles={styles.buttonStyle}
                      labelStyles={styles.buttonLabelStyle}
                    />
                  ) : null
                }

              </ListDivider>
              <Item label={this.getCollection(item.org_type).name} value={item.name} />
              <Item label={this.getCollection(item.org_type).address} value={item.address} />
              <Item label={this.getCollection(item.org_type).date} value={item.start_date} />
              {
                this.getCollection(item.org_type).expireDate ?
                  (
                    <Item label={this.getCollection(item.org_type).expireDate} value={item.expireDate} />
                  ) : null
              }
            </View>
          )}></FlatList>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  bottomContent: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 5
  },
  buttonStyle: {
    position: 'absolute',
    right: 2,
    justifyContent: 'center',
    //backgroundColor: 'transparent',
    height: 30,
    borderRadius: 5
  },
  buttonLabelStyle: {
    fontSize: 14
  }
})