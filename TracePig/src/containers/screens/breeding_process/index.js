import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';

import Item from '../../components/item';
import ListDivider from '../../components/list_divider';
import BusyIndicator from '../../components/indicator';

import farmService from '../../../services/farm.service';

import utils from '../../../helper/utils';

export default class BreedingProcess extends Component {
  state = {
    processing: []
  };
  lbCollections = {
    THUCAN: {
      lbTitle: 'Cho ăn',
      lbName: 'Tên thức ăn',
      lbAmount: 'Số lượng',
      lbActionDate: 'Ngày cho ăn'
    },
    THUOC: {
      lbTitle: 'Tiêm phòng',
      lbName: 'Tên vắc xin',
      lbAmount: 'Số mũi',
      lbActionDate: 'Ngày tiêm'
    }
  };

  componentDidMount() {
    this.getProcessing();
  }

  async getProcessing() {
    const response = await farmService.getBreedingProcess(this.props.code);
    const processing = response.items;
    Array.isArray(processing) && this.setState({ processing });
  }

  getLbCollection(key) {
    return this.lbCollections[key] || {
      lbTitle: '...',
      lbName: 'Tên',
      lbAmount: 'Số lượng',
      lbActionDate: 'Ngày'
    };
  }

  _renderItem = ({item}) => {
    let lbCollection = this.getLbCollection(item.p_type);
    return (
      <View>
        <ListDivider dividerText="">
          <Text style={{fontFamily: utils.fonts.nunitoBold, color: '#fff', fontSize: 16}}>Ngày: {item.ngay_cho_an + ' - ' + lbCollection.lbTitle}</Text> 
        </ListDivider>
        <Item label={lbCollection.lbName} value={item.item_name} numberOfLinesChanged={3}/>
        <Item label={lbCollection.lbAmount} value={item.so_luong} />
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <BusyIndicator showBackdrop={false} />
        <FlatList 
          data={this.state.processing}
          keyExtractor={(item, index) => index}
          renderItem = {this._renderItem}
        />
      </View>
    );
  }
}

