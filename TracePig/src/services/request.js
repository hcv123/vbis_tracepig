import config from '../../config';
import indicatorHandler from '../containers/components/indicator/indicator_handler'

const sendRequest = async (endpoint = '', method = 'get', params = {}, showIndicator) => {
  const FULL_URL = config.URL + endpoint;
  const REQUEST_CONFIG = {};
  let responseJson = {};
  
  REQUEST_CONFIG['headers'] = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }

  REQUEST_CONFIG['method'] = method;

  if (method === 'post' || method === 'patch') {
    REQUEST_CONFIG['body'] = JSON.stringify(params);
  }

  if(showIndicator) {
    indicatorHandler.showIndicator()
  }

  try {
    const response = await fetch(
      FULL_URL,
      REQUEST_CONFIG
    );  
    responseJson = response ? response.json() : {};
  } catch(err) {
    console.log(err);
  }

  if(showIndicator) {
    indicatorHandler.hideIndicator();
  }
  
  return responseJson;
}

export default {
  sendRequest
}