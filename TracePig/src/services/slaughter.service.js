import request from './request';

const getSlaughteredProcess = async (code) => {
    const result = await request.sendRequest(`slaughter/${code}`, 'get', {}, true);
    return result;
}

export default {
    getSlaughteredProcess
}