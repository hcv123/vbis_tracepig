import request from './request';

const getBreedingProcess = async (code) => {
  const result = await request.sendRequest(`farm/${code}`, 'get', {}, true);
  return result;
}

export default {
  getBreedingProcess
}