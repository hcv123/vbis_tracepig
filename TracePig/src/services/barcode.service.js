import request from './request';

const getBarcodeInfo = async (code) => {
  const result = await request.sendRequest(`animal/${code}`, 'get', {}, true);
  return result;
}

export default {
  getBarcodeInfo
}