const fonts = {
  nunitoRegular: 'Nunito-Regular',
  nunitoBold: 'Nunito-Bold'
}
const timeFormat = (date) => {
  const _date = new Date(date);
  const _hours = _date.getHours() > 9 ? _date.getHours() : '0' + _date.getHours();
  const _minutes = _date.getMinutes() > 9 ? _date.getMinutes() : '0' + _date.getMinutes();
  return _hours + ':' + _minutes
}
const dateFormat = (date = new Date()) => {
  const _date = new Date(date);
  return _date.getDate() + '/' + _date.getMonth() + '/' + _date.getFullYear(); 
}
const dateTimeFormat = (date = new Date()) => {
  const _date = new Date(date);
  return _date.getDate() + '/' + (_date.getMonth() + 1) + '/' + _date.getFullYear() + ' ' + timeFormat(_date);
}

export default {
  fonts,
  dateFormat,
  dateTimeFormat
}