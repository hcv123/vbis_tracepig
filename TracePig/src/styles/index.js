import { StyleSheet } from 'react-native';
import utils from '../helper/utils'

export const appStyles = {
  container: {
    flex: 1,
    backgroundColor:  'white',
    paddingRight: 5
  }
}


export const navigatorStyle = {
  navBarTextFontSize: 18,
  navBarTitleTextCentered: true,
  navBarButtonFontSize: 18,
  statusBarColor: 'green',
  navBarTextFontFamily: utils.fonts.nunitoBold,
  navBarBackgroundColor: 'green',
  navBarTextColor: '#fff',
  navBarButtonColor: '#fff'
}