import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/containers/screens';
import { navigatorStyle } from './src/styles'

registerScreens();

Navigation.startSingleScreenApp({
  screen: {
    screen: 'app.HomeScreen', // unique ID registered with Navigation.registerScreen
    title: 'Quét mã QR', // title of the screen as appears in the nav bar (optional)
    navigatorStyle: {
      ...navigatorStyle,
      navBarHidden: true
    }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
  },
  animationType: 'fade'
});

